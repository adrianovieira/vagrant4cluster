#!/bin/bash

cat <<EOF > /etc/docker/daemon.json
{
  "insecure-registries" : ["registry-bin.ebc", "registry.hackerlab"]
}
EOF

systemctl restart docker

if [[ "$?" == 0 ]]; then
  echo "INFO: [$(basename $0)] finished successfuly"
else
  exit 1
fi
