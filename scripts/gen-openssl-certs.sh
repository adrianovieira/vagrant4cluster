#!/bin/bash

CERT_DIR=/etc/ssl/certs
openssl req \
  -subj "/C=BR/ST=Distrito Federal/L=Brasilia/O=HackerLabs/OU=TIC Labs/CN=registry.hackerlab" \
  -newkey rsa:2048 -nodes -keyout $CERT_DIR/registry.harckerlab.key \
  -x509 -days 36500 -out $CERT_DIR/registry.harckerlab.crt
