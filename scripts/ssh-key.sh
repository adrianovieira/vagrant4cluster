#!/bin/sh -x
if [[ "$1" -eq '' ]]; then
  SERVER_NAME="server.mylab"
else
  SERVER_NAME=$1
fi

if [[ "$(hostname)" == "$SERVER_NAME" ]]; then
  #gen ssh keys pairs
  if [[ ! -f /root/.ssh/id_rsa ]]; then
    sudo -u root -i /bin/bash -l -c 'ssh-keygen -q -t rsa -b 4096 -C "root@server-master.mylab" -f ~/.ssh/id_rsa -N ""'
  fi
  sudo -u root -i /bin/bash -l -c 'cp -f ~/.ssh/id_rsa.pub /vagrant/root-id_rsa.pub'
  if [[ ! -f /home/vagrant/.ssh/id_rsa ]]; then
    sudo -u vagrant -i /bin/bash -l -c 'ssh-keygen -q -t rsa -b 4096 -C "vagrant@server-master.mylab" -f ~/.ssh/id_rsa -N ""'
  fi
  sudo -u vagrant -i /bin/bash -l -c 'cp -f ~/.ssh/id_rsa.pub /vagrant/vagrant-id_rsa.pub'
else
  sudo -u root -i /bin/bash -l -c 'mkdir -p ~/.ssh; umask 077; cat /vagrant/root-id_rsa.pub >> ~/.ssh/authorized_keys'
  sudo -u vagrant -i /bin/bash -l -c 'mkdir -p ~/.ssh; umask 077; cat /vagrant/vagrant-id_rsa.pub >> ~/.ssh/authorized_keys'
fi
