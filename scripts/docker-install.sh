#!/bin/bash

if [[ -f /etc/os-release  ]]; then
  . /etc/os-release
fi

if [ "$1" != "" ]; then
  DOCKER_VERSION=$1
fi

echo "INFO: [$(basename $0)] install docker on ($ID)"
if [[ "$ID" == "centos" ]]; then
  yum install -y yum-utils
  yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
  yum install -y docker-ce${DOCKER_VERSION} docker-ce-cli${DOCKER_VERSION} libseccomp containerd.io

elif [[ "$ID" == "ubuntu" ]]; then
  curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
  add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
  apt update
  apt install -y docker-ce${DOCKER_VERSION} docker-ce-cli${DOCKER_VERSION}
fi

if [[ "$?" == 0 ]]; then
  echo "INFO: [$(basename $0)] docker installed successfuly"
else
  exit 1
fi

echo "INFO: [$(basename $0)] Install docker-compose"
COMPOSEVERSION=$(curl -s https://github.com/docker/compose/releases/latest/download 2>&1 | grep -Po [0-9]+\.[0-9]+\.[0-9]+)
curl -sSL "https://github.com/docker/compose/releases/download/$COMPOSEVERSION/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
echo "INFO: [$(basename $0)] docker-compose $(docker-compose --version) installed"

# setup vagrant as user docker
echo "INFO: [$(basename $0)] setup docker on ($ID)"
if [[ "$ID" == "centos" ]]; then
  echo ""

elif [[ "$ID" == "ubuntu" ]]; then
  export LC_ALL=en_US
fi

systemctl enable docker
systemctl start docker
usermod -aG docker vagrant

docker --version
if [[ "$?" == 0 ]]; then
  echo "INFO: [$(basename $0)] finished successfuly"
else
  exit 1
fi
