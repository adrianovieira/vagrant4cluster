#!/bin/sh
# an alternative cmd to, e.g.:
#    sudo firewall-cmd --zone=public --add-port=80/tcp --add-port=8080/tcp --add-port=8443/tcp
iptables -A INPUT -p tcp -m tcp --dport 80 -j ACCEPT
iptables -A INPUT -p tcp -m tcp --dport 8080 -j ACCEPT
iptables -A INPUT -p tcp -m tcp --dport 500 -j ACCEPT
iptables -A INPUT -p udp -m udp --dport 500 -j ACCEPT
iptables -A INPUT -p tcp -m tcp --dport 4500 -j ACCEPT
iptables -A INPUT -p udp -m udp --dport 4500 -j ACCEPT
iptables -A INPUT -p tcp -m tcp --dport 9345 -j ACCEPT
iptables -A INPUT -p tcp -m tcp --dport 4789 -j ACCEPT
#k8s
iptables -A INPUT -p tcp -m tcp --dport 10250 -j ACCEPT
iptables -A INPUT -p tcp -m tcp --dport 10255 -j ACCEPT
iptables -A INPUT -p tcp -m tcp --dport 30000:32767 -j ACCEPT
if [ $(sysctl -n net.ipv4.ip_forward) -eq 0 ]; then sysctl -w net.ipv4.ip_forward=1; fi

#systemctl stop firewalld
