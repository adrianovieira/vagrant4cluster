#!/bin/sh
if [[ -f /etc/os-release  ]]; then
  . /etc/os-release
fi

echo "INFO: [$(basename $0)] sshd setup ($ID)"
sed -e 's/^PasswordAuthentication no/#PasswordAuthentication no/' -i /etc/ssh/sshd_config;
systemctl restart sshd
