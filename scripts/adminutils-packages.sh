#!/bin/sh

if [[ -f /etc/os-release  ]]; then
  . /etc/os-release
fi

echo "INFO: [$(basename $0)] install admin utils ($ID)"
if [[ "$ID" == "centos" ]]; then
  yum install -y epel-release
  yum install -y net-tools htop lsof bind-utils

elif [[ "$ID" == "ubuntu" ]]; then
  apt update
  apt install -y net-tools htop lsof dnsutils
fi
