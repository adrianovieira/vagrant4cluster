# -*- mode: ruby -*-
# vi: set ft=ruby :
'''
/**
 * Virtualization
 *
 * Vagrantfile for Linux Virtualization clustering
 * plugins:
 * - vagrant-hostmanager
 * - vagrant-libvirt
 * - vagrant-proxyconf
 * @author Adriano Vieira <adriano.svieira at gmail.com>

   Copyright 2016 Adriano dos Santos Vieira

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 * @license @see LICENCE
 */
 '''

require 'yaml'

cfg_data = YAML.load_file(File.join(File.dirname(__FILE__), 'vagrant-config.yaml'))

Vagrant.configure("2") do |config|
  config.vm.box = cfg_data.fetch('box')

  ### example on how to use a provisioner's script
  # config.vm.provision :shell,
  #   path: "scripts/sshd-setup.sh"

  if Vagrant.has_plugin?("vagrant-hostmanager")
    config.hostmanager.enabled = true
    config.hostmanager.manage_host = true
    config.hostmanager.manage_guest = true
    config.hostmanager.ignore_private_ip = false
    config.hostmanager.include_offline = true
  end

  config.vm.provider :virtualbox do |v, override|
    v.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
    v.customize ["modifyvm", :id, "--groups", "/vagrant"]
    override.vm.network "private_network", type: "dhcp"
    override.vm.synced_folder './', '/vagrant'
  end

  config.vm.provider :libvirt do |libvirt, override|
    override.vm.synced_folder './', '/vagrant', type: 'rsync'
  end

  config.vm.define "server-01" do |server|
    nodename = "server-01"
    cfg_server = cfg_data.fetch('server')

    if Vagrant.has_plugin?("vagrant-hostmanager") and cfg_server.fetch('fqdn_hosts').length > 0
      server.hostmanager.aliases = cfg_server.fetch('fqdn_hosts')
    end

    server.vm.hostname = nodename + cfg_data.fetch('domain')

    server.vm.provider :virtualbox do |v, override|
      v.cpus = cfg_server.fetch('cpus')
      v.memory = cfg_server.fetch('memory')
      v.name = nodename
      if Vagrant.has_plugin?("vagrant-hostmanager")
        override.hostmanager.ip_resolver = proc do |vm, resolving_vm|
          if hostname = (vm.ssh_info && vm.ssh_info[:host])
            `vagrant ssh #{nodename} -c "hostname -I"`.split()[1]
          end
        end
      end
    end

    server.vm.provider :libvirt do |libvirt, override|
      libvirt.cpus = cfg_server.fetch('cpus')
      libvirt.memory = cfg_server.fetch('memory')
    end

  end

  (1..cfg_data.fetch('node').fetch('count')).each do |i|
    cfg_node = cfg_data.fetch('node')
    nodename = "node-%02d" % i

    config.vm.define nodename do |node|

      if Vagrant.has_plugin?("vagrant-hostmanager") and cfg_node.fetch('fqdn_hosts').length > 0
        node.hostmanager.aliases = cfg_node.fetch('fqdn_hosts')
      end

      node.vm.hostname = nodename + cfg_data.fetch('domain')

      node.vm.provider :virtualbox do |v, override|
        v.customize ["modifyvm", :id, "--groups", "/vagrant"]
        v.cpus = cfg_node.fetch('cpus')
        v.memory = cfg_node.fetch('memory')
        v.name = nodename
        if Vagrant.has_plugin?("vagrant-hostmanager")
          override.hostmanager.ip_resolver = proc do |vm, resolving_vm|
            if hostname = (vm.ssh_info && vm.ssh_info[:host])
              `vagrant ssh #{nodename} -c "hostname -I"`.split()[1]
            end
          end
        end
      end

      node.vm.provider :libvirt do |libvirt|
        libvirt.cpus = cfg_node.fetch('cpus')
        libvirt.memory = cfg_node.fetch('memory')
      end

    end
  end

end
